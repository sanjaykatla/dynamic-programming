import java.util.Arrays;

public class MaxSumOfNonAdjacent {
    public static void main(String[] args) {

        int[] arr = {1, 3,5,7,3,2, 4};
        int[] dp = new int[arr.length];
        Arrays.fill(dp, -1);
        System.out.println(maxSum(arr.length - 1, arr, dp));
    }

    private static int maxSum(int ind, int[] arr, int[] dp) {

        if (ind == 0) {
            return arr[0];
        }
        if(ind < 0){
            return 0;
        }

        if(dp[ind] != -1) return dp[ind];

        int res = Math.max(arr[ind] + maxSum(ind - 2, arr, dp), arr[ind - 1]);
        dp[ind] = res;
        return res;
    }
}
