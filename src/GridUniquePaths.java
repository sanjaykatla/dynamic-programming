public class GridUniquePaths {
    private static int recursion(int i, int j){

        if( i == 0 && j == 0 ){
            return 1;
        }
        if( i < 0 || j < 0 ){
            return 0;
        }

        int top = recursion(i-1,j);
        int left = recursion(i, j-1);

        return top + left;
    }

    private static int dpTopDown(int i, int j, int[][] dp){
        if(i == 0 && j == 0 ){
            return 1;
        }

        if(i < 0 || j < 0 ){
            return 0;
        }

        if(dp[i][j] != -1) return dp[i][j];

        int top = recursion(i-1,j);
        int left = recursion(i, j-1);
        dp[i][j] = top + left;
        return dp[i][j];
    }

    private static int dpBottomUp(int m, int n) {

        int[][] dp = new int[m + 1][n + 1];
        for (int i = 1; i < m + 1; i++) {
            for (int j = 1; j < n + 1; j++) {
                if( i == 1 && j == 1) dp[i][j] = 1;
                else dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
            }
        }
        return dp[m][n];
    }

    private static int dpBottomUpSpaceOptimized(int m, int n) {

        int[] dp = new int[n];
        for (int i = 0; i < m; i++) {
            int[] temp = new int[n];
            for (int j = 0; j < n; j++) {
                if(j == 0) temp[j] = 1;
                else temp[j] = dp[j] + temp[j - 1];
            }
            dp = temp;
        }
        return dp[n-1];
    }

    public static void main(String[] args) {
        int m = 5;
        int n = 4;

        long recStartTime = System.currentTimeMillis();
        int recRes = recursion(m-1, n-1);
        long recEndTime = System.currentTimeMillis();
        System.out.println("Rec result: "+ recRes+" Time taken: "+ (recEndTime - recStartTime));

        int[][] dp = new int[m][n];
        for(int i=0; i<m; i++){
            for(int j=0; j<n; j++){
                dp[i][j] = -1;
            }
        }

        long dpStartTime = System.currentTimeMillis();
        int res = dpTopDown(m-1, n-1, dp);
        long dpEndTime = System.currentTimeMillis();
        System.out.println("Dp result: "+ res+" Time taken: "+ (dpEndTime - dpStartTime));

        System.out.println("Bottom-Up: "+ dpBottomUp(m, n));
        System.out.println("Bottom-Up space optimized: "+ dpBottomUpSpaceOptimized(m, n));
    }
}
