public class MaxPointsNinja {

    public static void main(String[] args) {

        int[][] points = {
                {30, 5, 20},
                {30, 5, 20},
                {30, 5, 20},
                {100, 1, 2},
                {500, 300, 2}
        };
        int[][] dp = new int[points.length][points[0].length+1];
        for(int i=0; i<points.length;i++){
            for(int j=0; j<=points[0].length; j++){
                dp[i][j] = -1;
            }
        }

        long recStartTime = System.currentTimeMillis();
        int recRes = rec(points.length-1, points[0].length, points);
        long recEndTime = System.currentTimeMillis();
        System.out.println("Rec result: "+ recRes+" Time taken: "+ (recEndTime - recStartTime));

        long dpStartTime = System.currentTimeMillis();
        int res = dpTopDown(points.length-1, points[0].length, points, dp);
        long dpEndTime = System.currentTimeMillis();
        System.out.println("Dp result: "+ res+" Time taken: "+ (dpEndTime - dpStartTime));
    }


    private static int dpTopDown(int day, int last, int[][] points, int[][] dp) {

        if(day == 0){
            int max = Integer.MIN_VALUE;
            for(int j=0; j<points[day].length; j++){
                if( j != last){
                    max = Math.max(max, points[day][j]);
                }
            }
            return max;
        }

        if(dp[day][last] != -1) return dp[day][last];

        int max = Integer.MIN_VALUE;
        for(int act=0; act < points[day].length; act++){
            if(act != last){
                max =  Math.max(max, points[day][act]+ dpTopDown(day-1, act, points, dp));
            }
        }
        dp[day][last] = max;
        return max;
    }

    private static int rec(int day, int last, int[][] points) {

        if(day == 0){
            int max = Integer.MIN_VALUE;
            for(int j=0; j<points[day].length; j++){
                if( j != last){
                    max = Math.max(max, points[day][j]);
                }
            }
            return max;
        }

        int max = Integer.MIN_VALUE;
        for(int act=0; act < points[day].length; act++){
            if(act != last){
                max =  Math.max(max, points[day][act]+ rec(day-1, act, points));
            }
        }
        return max;
    }
}
